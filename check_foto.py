import os
from tkinter import Image
from PIL import Image, ImageChops
import datetime

import config
import data_base_api


def dhash(path_image, hash_size=8):
    temp_image = Image.open(path_image)
    image = temp_image.convert('L').resize(
        (hash_size + 1, hash_size),
        Image.Resampling.LANCZOS,
    )
    pixels = list(image.getdata())
    difference = []
    for row in range(hash_size):
        for col in range(hash_size):
            pixel_left = image.getpixel((col, row))
            pixel_right = image.getpixel((col + 1, row))
            difference.append(pixel_left > pixel_right)
    decimal_value = 0
    hex_string = []
    for index, value in enumerate(difference):
        if value:
            decimal_value += 2 ** (index % 8)
        if (index % 8) == 7:
            hex_string.append(hex(decimal_value)[2:].rjust(2, '0'))
            decimal_value = 0
    return ''.join(hex_string)


if __name__ == '__main__':
    while True:
        today_date = datetime.datetime.now()
        dir_photo_list = os.listdir(os.path.join(config.temp_photo_folder))
        if len(dir_photo_list) > 0:
            for i_file in dir_photo_list:
                try:
                    user_id = i_file.split(' ')[0]
                    file_name = i_file.split(' ')[1]
                    file_path = os.path.join(config.temp_photo_folder, i_file)
                    file_id = file_name.split('.')[0]
                    result_hash = dhash(file_path)
                    if data_base_api.if_exists(result_hash) is None:
                        data_base_api.add_hash(result_hash)
                        data_base_api.add_user_photo(user_id, file_id, today_date.day,
                                                     today_date.month, today_date.year, 1)
                    else:
                        data_base_api.add_user_photo(user_id, file_id, today_date.day,
                                                     today_date.month, today_date.year, 0)
                    os.remove(os.path.join(config.temp_photo_folder, i_file))
                except BaseException:
                    pass
