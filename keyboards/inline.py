from aiogram.types import KeyboardButton, ReplyKeyboardMarkup, InlineKeyboardButton, InlineKeyboardMarkup
from datetime import datetime

month_dict = {
    1: "Январь",
    2: "Февраль",
    3: "Март",
    4: "апреля",
    5: "Май",
    6: "Июнь",
    7: "Июль",
    8: "Август",
    9: "Cентябрь",
    10: "Октябрь",
    11: "Ноябрь",
    12: "Декабрь"
}


def admin_button_inline():
    inline_btn_1 = InlineKeyboardButton('Статистика за сегодня', callback_data='stats_1')
    inline_btn_2 = InlineKeyboardButton('Статистика по дате', callback_data='stats_2')
    inline_btn_3 = InlineKeyboardButton('Изменить цену', callback_data='change_price')
    inline_btn_4 = InlineKeyboardButton('Статистика по месяцам', callback_data='month_stats')
    inline_kb_full = InlineKeyboardMarkup(row_width=2)
    inline_kb_full.add(inline_btn_1, inline_btn_2, inline_btn_3, inline_btn_4)
    return inline_kb_full


def date_inline_calendar(list_date):
    inline_day_kb = InlineKeyboardMarkup(row_width=7)
    date = datetime.now()
    for day in list_date:
        inline_day_kb.insert(
            InlineKeyboardButton(f'{day} / {date.month}', callback_data=f'day_{day}.{date.month}'))
    return inline_day_kb


def generate_workers_button(worker_list):
    inline_day_kb = InlineKeyboardMarkup(row_width=7)
    for worker in worker_list:
        inline_day_kb.insert(InlineKeyboardButton(f'{worker[1]} {worker[2]}', callback_data=f'{worker[0]}'))
    return inline_day_kb


def answer_button(user_id):
    inline_btn_1 = InlineKeyboardButton('Добавить', callback_data=f'add-worker_{user_id}')
    inline_btn_2 = InlineKeyboardButton('Отклонить', callback_data=f'cancel-worker_{user_id}')
    inline_kb_full = InlineKeyboardMarkup(row_width=2)
    inline_kb_full.add(inline_btn_1, inline_btn_2)
    return inline_kb_full


def del_worker(user_id):
    inline_day_kb = InlineKeyboardMarkup(row_width=2)
    inline_day_kb.insert(InlineKeyboardButton(f'Удалить', callback_data=f'del-worker_{user_id}'))
    return inline_day_kb


def get_month_markup():
    inline_kb_full = InlineKeyboardMarkup(row_width=2)
    for month_id, month_name in month_dict.items():
        inline_kb_full.insert(InlineKeyboardButton(month_name, callback_data=f"month-stats_{month_id}"))
    return inline_kb_full

