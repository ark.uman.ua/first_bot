import os
import sqlite3
from datetime import datetime
from sqlite3 import IntegrityError

import check_foto

BASE_DIR = os.path.dirname(os.path.abspath(__file__))
path_to_db = os.path.join(BASE_DIR, 'new_file_hash.db')


def create_db():
    with sqlite3.connect(path_to_db) as db:
        db.execute('create table if not exists hash_data '
                   '( hash text not null)')

        db.execute('create unique index IF NOT EXISTS hash_data_hash_uindex  '
                   'on hash_data (hash)')

        db.execute('create table if not exists price_data   '
                   ' (        price real not null    )')

        db.execute('create table if not exists user    '
                   '(user_id     INTEGER not null            primary key            unique,        '
                   'first_name  TEXT,        '
                   'second_name TEXT,        '
                   'is_worker   INTEGER default 0    )')

        db.execute('create table if not exists users_photo    '
                   '(user_id  INTEGER not null,        '
                   'photo_id TEXT,        day      INTEGER,        month    INTEGER,        year     INTEGER    , is_unique  integer )')

        create_default_price()
        db.commit()


def add_hash(hash):
    with sqlite3.connect(path_to_db) as db:
        db.execute("INSERT INTO hash_data "
                   "(hash) "
                   "VALUES (?)",
                   [hash])
        db.commit()


def add_user(user_id, first_name, second_name):
    with sqlite3.connect(path_to_db) as db:
        try:
            db.execute("INSERT INTO user "
                       "(user_id, first_name, second_name) "
                       "VALUES (?, ?, ?)",
                       [user_id, first_name, second_name])
            db.commit()
        except IntegrityError:
            pass


def set_worker(user_id):
    with sqlite3.connect(path_to_db) as db:
        db.execute("update user set is_worker = 1 where user_id = ?", (user_id,))
        db.commit()


def add_user_photo(user_id, photo_id, day, month, year, is_unique):
    with sqlite3.connect(path_to_db) as db:
        db.execute("INSERT INTO users_photo "
                   "(user_id, photo_id, day, month, year, is_unique) "
                   "VALUES (?, ?, ?, ?, ?, ?)",
                   [user_id, photo_id, day, month, year, is_unique])
        db.commit()


def get(user_id):
    with sqlite3.connect(path_to_db) as db:
        user = db.execute("SELECT * FROM user where user_id = ?", (user_id,)).fetchone()
    return user


def get_user_name(user_id):
    with sqlite3.connect(path_to_db) as db:
        user = db.execute("SELECT first_name, second_name FROM user where user_id = ?", (user_id,)).fetchone()
    return user


def get_user_photo(user_id, day, month, year, is_unique):
    with sqlite3.connect(path_to_db) as db:
        users_photo = db.execute("SELECT * FROM users_photo where day = ? and month = ? and year = ? and user_id = ?"
                                 " and is_unique = ?",
                                 (day, month, year, user_id, is_unique)).fetchall()
    return users_photo


def get_all_photo(day, month, year):
    with sqlite3.connect(path_to_db) as db:
        users_photo = db.execute("SELECT * FROM users_photo where day = ? and month = ? and year = ?",
                                 (day, month, year,)).fetchall()
    return users_photo


def get_all_users():
    with sqlite3.connect(path_to_db) as db:
        users = db.execute("SELECT user_id, first_name, second_name FROM user where is_worker = 1").fetchall()
    return users


def delete_user(user_id):
    with sqlite3.connect(path_to_db) as db:
        db.execute("DELETE from user where user_id = ?", (user_id,))
        db.commit()


def get_users_for_date_unique(day, month, year):
    with sqlite3.connect(path_to_db) as db:
        users = db.execute("SELECT user.user_id, count(photo_id), user.first_name, user.second_name "
                           " FROM users_photo "
                           "join user "
                           "ON user.user_id = users_photo.user_id "
                           "where day = ? and month = ? and year = ? and is_unique = 1 group by user.user_id",
                           (day, month, year)).fetchall()
    return users


def get_user_count_photo_for_date_not_unique(user_id, day, month, year):
    with sqlite3.connect(path_to_db) as db:
        count_user_photo = db.execute("SELECT count(photo_id) FROM users_photo where day = ? and month = ? and year = ?"
                                      " and user_id = ? and is_unique = 0",
                                      (day, month, year, user_id)).fetchone()
    return count_user_photo


def if_exists(new_hash):
    with sqlite3.connect(path_to_db) as db:
        hash = db.execute("SELECT hash FROM hash_data where hash = ?", (new_hash,)).fetchone()
    return hash


def create_default_price():
    with sqlite3.connect(path_to_db) as db:
        rice = db.execute("SELECT price FROM price_data").fetchone()
        if not rice:
            db.execute("INSERT INTO price_data "
                       "(price) "
                       "VALUES (?)",
                       [0.5])
            db.commit()


# удаляем прошлую цену с базы данных и ставим новую

def update_price(new_price):
    with sqlite3.connect(path_to_db) as db:
        db.execute('delete FROM price_data')
        db.commit()
        db.execute("INSERT INTO price_data "
                   "(price) "
                   "VALUES (?)",
                   [new_price])
        db.commit()


# получаем текущую цену
def get_price():
    with sqlite3.connect(path_to_db) as db:
        price = db.execute("SELECT price FROM price_data").fetchone()
    return price


def get_users_for_month_unique(month):
    with sqlite3.connect(path_to_db) as db:
        users = db.execute("SELECT user.user_id, count(photo_id), user.first_name, user.second_name "
                           " FROM users_photo "
                           "join user "
                           "ON user.user_id = users_photo.user_id "
                           "where month = ? and year = ?  and is_unique = 1 group by user.user_id",
                           (month, datetime.now().year,)).fetchall()
    return users


def get_user_count_photo_for_mouth_not_unique(user_id, month):
    with sqlite3.connect(path_to_db) as db:
        count_user_photo = db.execute("SELECT count(photo_id) FROM users_photo where month = ? and year = ?"
                                      " and user_id = ? and is_unique = 0",
                                      (month, datetime.now().year, user_id)).fetchone()
        return count_user_photo
