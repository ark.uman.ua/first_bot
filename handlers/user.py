import datetime
import os.path

import aiogram
from aiogram.dispatcher import FSMContext
from aiogram.types import Message

import config
import data_base_api
from config import dp, bot
from keyboards import reply
import check_foto
from handlers.echo import Form


@dp.message_handler(commands='start', state='*')
async def send_message(message: Message):
    if message.from_user.id in [i_user[0] for i_user in data_base_api.get_all_users()]:
        await bot.send_message(message.from_user.id,
                           text=f'Привет {message.from_user.username}.'
                                f'\nГрузи фото наклеенных листовок')
        return
    await bot.send_message(message.from_user.id,
                           text='Вы здесь не работаете. Если хотите зарегистрироваться как работник - отправьте'
                                ' свое имя и фамилию, по примеру Иван Иванов')
    await Form.name.set()


@dp.message_handler()
async def send_message(message: Message, state: FSMContext):
    if message.from_user.id in [i_user[0] for i_user in data_base_api.get_all_users()]:
        await bot.send_message(message.from_user.id, text='Ошибочный ввод. Нажмите /start')
        return
    await bot.send_message(message.from_user.id,
                           text='Вы здесь не работаете. Если хотите зарегистрироваться как работник - отправьте'
                                ' свое имя и фамилию, по примеру Иван Иванов')
    await Form.name.set()


@dp.message_handler(content_types=['photo'])
async def send_message(message: Message):
    if message.from_user.id in [i_user[0] for i_user in data_base_api.get_all_users()]:
        file_id = message.photo[-1].file_id
        file_data = await bot.get_file(file_id)
        temp_link = os.path.join(config.temp_photo_folder, f'{message.from_user.id} {file_id}.jpg')

        await bot.download_file(file_data.file_path, f'{temp_link}')
        await bot.send_message(message.from_user.id, text='Ваше фото принято. Спасибо')

    else:
        await bot.send_message(message.from_user.id, text='Ошибка: Вы не можете грузить фото, поскольку не являетесь'
                                                          ' работником')
