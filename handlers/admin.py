import asyncio
from datetime import datetime

import aiogram
from aiogram.dispatcher import FSMContext
from aiogram.dispatcher.filters.state import State, StatesGroup
from aiogram.types import Message, CallbackQuery, MediaGroup
from aiogram.utils.exceptions import BadRequest

import keyboards.inline
from keyboards.inline import month_dict
import config
import data_base_api
from config import dp, bot, create_time_list
from keyboards import reply, inline


# TODO Handlers reply buttons


class Form(StatesGroup):
    name = State()
    price = State()


async def send_data_to_admin(text):
    for admins in config.ADMIN_ID:
        try:
            await bot.send_message(admins, text=text)
        except Exception as e:
            pass


@dp.message_handler(aiogram.filters.IDFilter(chat_id=config.ADMIN_ID), commands=['admin', 'start'], state='*')
async def send_message(message: Message, state: FSMContext):
    await state.finish()
    await bot.send_message(message.from_user.id, text='Приветствую Админ',
                           reply_markup=reply.admin_button())


@dp.message_handler(aiogram.filters.IDFilter(chat_id=config.ADMIN_ID), text='Посмотреть статистику')
async def send_message(message: Message):
    cur_price = data_base_api.get_price()
    await bot.send_message(message.from_user.id, text=f'<b>Текущая цена:  {cur_price[0]}</b>')
    await bot.send_message(message.from_user.id, text='Статистика за какой период:',
                           reply_markup=inline.admin_button_inline())


@dp.message_handler(aiogram.filters.IDFilter(chat_id=config.ADMIN_ID), text='Список работников')
async def send_message(message: Message):
    message_text = 'Список работников: \n\n'
    for worker in data_base_api.get_all_users():
        message_text += f'{worker[1]} {worker[2]}     /delete_{worker[0]}\n'
    await bot.send_message(message.from_user.id, text=message_text)


@dp.callback_query_handler(aiogram.filters.IDFilter(chat_id=config.ADMIN_ID), text='stats_1')
async def get_chest_callback(call: CallbackQuery):
    await call.answer()
    cur_price = data_base_api.get_price()[0]
    date = datetime.today()
    message_text = 'Статистика за сегодня\n'
    for worker in data_base_api.get_users_for_date_unique(date.day, date.month, date.year):
        not_unique = data_base_api.get_user_count_photo_for_date_not_unique(worker[0], date.day, date.month, date.year)[0]
        message_text += f"{worker[2]} {worker[3]}:\n" \
                        f"загружено фото - <b>{worker[1] + not_unique}</b> шт.\n" \
                        f"из них уникальных - {worker[1]} шт. \n" \
                        f"не уникальных - {not_unique} шт. \n" \
                        f"заработок составил - <b>{worker[1] * cur_price}</b> грн.\n" \
                        f"смотреть уникальные /photo_{worker[0]}_{date.day}_{date.month}_{date.year}_1\n" \
                        f"смотреть не уникальные /photo_{worker[0]}_{date.day}_{date.month}_{date.year}_0\n\n"
    await bot.send_message(call.from_user.id, text=message_text)
    if 'photo' not in message_text:
        await bot.send_message(call.from_user.id, 'Фото за сегодня нет')


@dp.callback_query_handler(aiogram.filters.IDFilter(chat_id=config.ADMIN_ID), text='stats_2')
async def get_chest_callback(call: CallbackQuery):
    await call.answer()
    await bot.send_message(call.from_user.id, text=f'Выберите дату',
                           reply_markup=inline.date_inline_calendar(create_time_list()))


# изменение ценны
@dp.callback_query_handler(aiogram.filters.IDFilter(chat_id=config.ADMIN_ID), text='change_price')
async def get_chest_callback(call: CallbackQuery):
    await call.answer()
    await bot.send_message(call.from_user.id, text=f'Введите новую цену')
    await Form.price.set()


@dp.message_handler(aiogram.filters.IDFilter(chat_id=config.ADMIN_ID), state=Form.price)
async def add_new_worker(message: Message, state: FSMContext):
    try:
        new_price = float(message.text)
    except ValueError:
        await bot.send_message(message.from_user.id, 'Пожалуйста введите новую цену!')
        return
    data_base_api.update_price(new_price)
    await state.finish()
    cur_price = data_base_api.get_price()[0]
    await bot.send_message(message.from_user.id, text='Цена изменена')
    await send_data_to_admin(f'<b>Изменение цены!\nТекущая цена: {cur_price}</b>')
    # for user in data_base_api.get_all_users():
    #     await bot.send_message(user[0], text=f'<b>Внимание! Изменение цены!\nТекущая цена: {cur_price}</b>')


@dp.message_handler(aiogram.filters.IDFilter(chat_id=config.ADMIN_ID), text_startswith='/delete')
async def del_worker(message: Message):
    worker_id = message.text.split('_')[-1]
    user_data = data_base_api.get(worker_id)
    data_base_api.delete_user(worker_id)
    await send_data_to_admin(f'Работник {user_data[1]} {user_data[2]} удален')
    await bot.send_message(worker_id, text='Вас уволили')


@dp.callback_query_handler(aiogram.filters.IDFilter(chat_id=config.ADMIN_ID), text_startswith='day_')
async def get_chest_callback(call: CallbackQuery):
    await call.answer()
    cur_price = data_base_api.get_price()[0]
    today = datetime.today()
    date = call.data.split('_')[-1].split('.')
    day, month = date[0], date[1]
    message_text = f'Статистика за   {day}/{month}:\n'
    for worker in data_base_api.get_users_for_date_unique(day, month, today.year):
        not_unique = data_base_api.get_user_count_photo_for_date_not_unique(worker[0], day, month, today.year)[0]
        message_text += f"{worker[2]} {worker[3]}:\n" \
                        f"загружено фото - <b>{not_unique + worker[1]}</b> шт.\n" \
                        f"из них уникальных - {worker[1]} шт.\n" \
                        f"не уникальных - {not_unique} шт.\n" \
                        f"заработок составил - <b>{worker[1] * cur_price}</b> грн.\n" \
                        f"смотреть уникальные /photo_{worker[0]}_{day}_{month}_{today.year}_1\n" \
                        f"смотреть не уникальные /photo_{worker[0]}_{day}_{month}_{today.year}_0\n\n"
    await bot.send_message(call.from_user.id, text=message_text)
    if 'photo' not in message_text:
        await bot.send_message(call.from_user.id, 'Фото за выбранную дату нет')


@dp.message_handler(aiogram.filters.IDFilter(chat_id=config.ADMIN_ID), text_startswith='/photo')
async def photo_download_command(message: Message):
    try:
        # пробуем разбить команду на параметры, нам нужно что бы нам передали юзер айди и дату
        command_data_list = message.text.split('_')
        target_user_id, day, month, year, is_unique = command_data_list[1], command_data_list[2], \
                                           command_data_list[3], command_data_list[4], command_data_list[5]
    except IndexError:
        # если параметров не хватет - кидаем ошибку
        await bot.send_message(message.from_user.id, 'Команда не верна!')
        return
    # получаем список фото пользователя
    user_photo_list = data_base_api.get_user_photo(target_user_id, day, month, year, is_unique)
    if user_photo_list == []:
        await bot.send_message(message.from_user.id, 'Фото нет')
        return
    media = MediaGroup()
    counter = 0
    for data in user_photo_list:

        media.attach_photo(photo=data[1])
        counter += 1
        if counter > 9:
            try:
                await bot.send_media_group(message.from_user.id, media)
                await asyncio.sleep(1)
            # обработка неверного file_id, бывает когда меняют токен бота
            except BadRequest:
                pass
            counter = 0
            media = MediaGroup()
    if counter > 0:
        await bot.send_media_group(message.from_user.id, media)


@dp.message_handler(aiogram.filters.IDFilter(chat_id=config.ADMIN_ID), text_startswith='/delete')
async def del_worker(message: Message):
    worker_id = message.text.split('_')[-1]
    user_data = data_base_api.get(worker_id)
    data_base_api.delete_user(worker_id)
    await send_data_to_admin(f'Работник {user_data[1]} {user_data[2]} удален')
    await bot.send_message(worker_id, text='Вас уволили')


@dp.callback_query_handler(text_startswith='add-worker_')
async def new_worker(call: CallbackQuery):
    worker_id = call.data.split('_')[-1]
    data_base_api.set_worker(worker_id)
    user_data = data_base_api.get(worker_id)
    await call.answer('Все ок', show_alert=True)
    await call.message.delete()
    await send_data_to_admin(f'Работник {user_data[1]} {user_data[2]} добавлен')
    await bot.send_message(worker_id, text='Ваша заявка одобрена')


@dp.callback_query_handler(text_startswith='cancel-worker_')
async def new_worker(call: CallbackQuery):
    worker_id = call.data.split('_')[-1]
    data_base_api.delete_user(worker_id)
    user_data = data_base_api.get(worker_id)
    await call.answer('Все ок', show_alert=True)
    await call.message.delete()
    await send_data_to_admin(f'Работник {user_data[1]} {user_data[2]} отклонен')
    await bot.send_message(worker_id, text='Ваша заявка отклонена')


@dp.callback_query_handler(text='month_stats')
async def get_month_markup(call: CallbackQuery):
    await call.answer()
    await bot.send_message(call.from_user.id,
                           text='Пожалуйста выберите месяц этого года за который хотите получить статистику',
                           reply_markup=keyboards.inline.get_month_markup())


@dp.callback_query_handler(text_startswith='month-stats_')
async def month_stats(call: CallbackQuery):
    await call.answer()
    cur_price = data_base_api.get_price()[0]
    month_id = int(call.data.split('_')[-1])
    message_text = f'Статистика за {month_dict[month_id]}:\n'
    users_data = data_base_api.get_users_for_month_unique(month_id)
    if users_data:
        for worker in users_data:
            count_not_unique = data_base_api.get_user_count_photo_for_mouth_not_unique(worker[0], month_id)[0]
            message_text += f"{worker[2]} {worker[3]}:\n" \
                            f"загружено фото - {worker[1] + count_not_unique} шт.\n" \
                            f"из них уникальных - {worker[1]} шт.\n" \
                            f"не уникальных - {count_not_unique} шт.\n" \
                            f"заработок составил - <b>{worker[1] * cur_price}</b> грн.\n\n"
        await bot.send_message(call.from_user.id, text=message_text)
        return
    await bot.send_message(call.from_user.id, 'Данных за выбранный месяц нет')
