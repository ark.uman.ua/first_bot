from aiogram.utils import executor
from handlers import admin, user, echo
from data_base_api import create_db
from config import dp

if __name__ == '__main__':
    create_db()
    executor.start_polling(dp)

